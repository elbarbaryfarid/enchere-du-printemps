/*!
* Start Bootstrap - Simple Sidebar v6.0.6 (https://startbootstrap.com/template/simple-sidebar)
* Copyright 2013-2023 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-simple-sidebar/blob/master/LICENSE)
*/
// 
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
         if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
             document.body.classList.toggle('sb-sidenav-toggled');
             if(document.body.classList.contains('sb-sidenav-toggled')){
				sidebarToggle.textContent = "Réduire";
				sidebarToggle.title = "Réduire le menu";
			} else {
				sidebarToggle.textContent = "Afficher";
				sidebarToggle.title = "Affiche le menu";
			}
         }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            if(document.body.classList.contains('sb-sidenav-toggled')){
				sidebarToggle.textContent = "Réduire";
				sidebarToggle.title = "Réduire le menu";
			} else {
				sidebarToggle.textContent = "Afficher";
				sidebarToggle.title = "Affiche le menu";
			}
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }
});
