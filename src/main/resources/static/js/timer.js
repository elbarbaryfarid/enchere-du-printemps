		window.addEventListener('DOMContentLoaded', event => {
			function dateTiming(toDate, element) {
				var dateTo = new Date(toDate);
				setInterval(function(){
					dateApply(dateTo, element)
				} , 1000);
			}
			function dateApply(toDate, element) {
				var dateNow = new Date();
				var difference = toDate.getTime() - dateNow.getTime();
				const secs = Math.floor(Math.abs(difference) / 1000);
				var d = new Date(secs * 1000);
				var days = "";
				console.log(d.getDate());
				if(d.getDate() > 1){
					
					days = (d.getDate()-1) < 10 ? "0"+(d.getDate()-1) : (d.getDate()-1);
				}
				element.textContent = days == "" ? d.toISOString().substring(11, 19) : days + ":" + d.toISOString().substring(11, 19);
			}
			const elements = document.querySelectorAll(".diff");
			if(elements){
				for (var i = 0; i < elements.length; i++) {
					var l = elements[i];
					var date = l.getAttribute("data-date");
					dateTiming(date, l);
				}
			}
			
		});