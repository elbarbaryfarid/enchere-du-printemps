package fr.printemps.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import fr.printemps.entities.Role;
import fr.printemps.entities.User;
import fr.printemps.repositories.UserRepository;
import fr.printemps.storage.files.StorageService;

@Controller
public class AuthController {
		
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private StorageService storageService;

    @PostMapping("/register")
    public String registerUser(@RequestParam("image") MultipartFile image, @ModelAttribute User newly, Model model){
    	String redirect = "redirect:/";
    	List<String> errors = new ArrayList<>();
        if(userRepository.existsByUsername(newly.getUsername())){
        	errors.add("Nom d'utilisateur déjà utilisé !");
        }
        if(userRepository.existsByEmail(newly.getEmail())){
        	errors.add("Email déjà utilisé !");
        }
        if(errors.size() > 0) {model.addAttribute("errors_register",errors);return "signin";}
        if(image != null && !image.isEmpty()) {
        	newly.setProfilPicture(storageService.store(image));
        }
        newly.setPassword(passwordEncoder.encode(newly.getPassword()));        
        Role role = new Role("ROLE_USER");
        role.setUsername(newly);
        java.util.Set<Role> set = new HashSet<Role>();
        set.add(role);
        newly.setRoles(set);
        newly.setEnabled(true);
        userRepository.save(newly);
        return redirect;
    }
    
	/**
	 * Renvoie la vue permettant l'inscription d'un utilisateur
	 * 
	 * @param redirect
	 * @param model
	 * @return
	 */
	@GetMapping("/signup")
	public String AfficherSignUp(@RequestParam(required = false, name = "redirect") String redirect, Model model) {
		model.addAttribute("newly", new User());
		if(redirect != null && redirect.trim() != "") {
			model.addAttribute("redirect", redirect);
			
		}
		return "signin";
	}
	
	/**
	 * Renvoie la vue permettant l'authentification d'un utilisateur
	 * 
	 * @param redirect
	 * @param model
	 * @return
	 */
	@GetMapping("/signin")
	String AfficherSignIn(@RequestParam(required = false) String redirect, Model model) {
		model.addAttribute("newly", new User());
		if (redirect != null && redirect.trim() != "") {
			model.addAttribute("redirect", redirect);
			model.addAttribute("newly", new User());
		}
		return "signin";
	}
}
