package fr.printemps.controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class ErreurController implements ErrorController {
	
	
	/**
	 * Mapping automatiquement appelé par spring lorsque la réponse d'une requête a un code d'erreur
	 * @param request
	 * @param model
	 * @return
	 */
	@GetMapping("/error")
	public String handleError(HttpServletRequest request, Model model) {
		System.out.println("test");
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		String redirect = "error";
		if(status != null) {
			int codeStatus = Integer.parseInt(status.toString());
			switch (codeStatus) {
			case 404: {
				model.addAttribute("code", "404");
				model.addAttribute("msgErr", "Page introuvable");
				break;
			}
			case 401:
				model.addAttribute("code", "401");
				model.addAttribute("msgErr", "Vous n'avez pas les droits nécessaires pour lire cette page");
				break;
			case 403:
				redirect = "redirect:/signin";
				break;
			case 500:
				model.addAttribute("code", "500");
				model.addAttribute("msgErr", "Erreur interne (erreur 500)");
				break;
			default:
				model.addAttribute("code", "");
				model.addAttribute("msgErr", "Une erreur est survenue, merci de contacter le support");
				break;
			}
		}
		return redirect;
	}
}
