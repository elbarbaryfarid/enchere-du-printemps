package fr.printemps.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import fr.printemps.entities.User;
import fr.printemps.services.UserService;
import fr.printemps.storage.files.StorageService;
import fr.printemps.tools.DtoTool;

@Controller
@RequestMapping("/profil")
public class UserController {
	
	private final UserService userService;
	
	@Autowired
	private StorageService storageService;
	
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
//	private List<UserDto> convertListToDto(List<User> users){
//		List<UserDto> res = new ArrayList<>();
//		for(User u : users) {
//			res.add(DtoTool.convert(u, UserDto.class));
//		}
//		return res;
//	}
	
	@GetMapping
	public String defaultEntry(Model model) {
		return "redirect:/profil/me";
	}

	@GetMapping("/")
	public String defaultEntryN(Model model) {
		return "redirect:/profil/me";
	}
	
	
	/**
	 * Route menant vers la page personnel d'un utilisateur connecté, ou redirige
	 * vers le contrôleur d'erreur si l'API nous indique un accès non autorisé
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@GetMapping("/me")
	public String MonProfil(Model model) {
		return "myprofil";
	}

	@GetMapping(value= {"/{username}"})
	public String getPublicUserInfo(@PathVariable(value = "username", required = true) String username, Model model) {
//        TypeMap<User, UserDto> typeMap = modelMapper.createTypeMap(User.class, UserDto.class);
//    	typeMap.addMappings(mapper -> mapper.skip(UserDto::setSolde));
//    	typeMap.addMappings(mapper -> mapper.skip(UserDto::setFirstName));
//    	typeMap.addMappings(mapper -> mapper.skip(UserDto::setLastName));
//    	typeMap.addMappings(mapper -> mapper.skip(UserDto::setTransactions));
//    	UserDto dto = modelMapper.map(userService.findByUsername(username), UserDto.class);
		Optional<User> optUser = userService.findByUsername(username);
		if(optUser.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		User user = optUser.get();
        model.addAttribute("msg", user.toString());
		return "profil";
	}
	
	@GetMapping(value="/profil/me/lots")
	public String getMesLots() {
		return "";
	}
	
	@PostMapping("/profil/update")
	public String setProfilData(Authentication authentification, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("email") String email
			, @RequestParam("picture") MultipartFile picture) {
		return "";
	}
	
	@GetMapping("/me/lots")
	public String getLotsUser(Model model) {
		
		return "";
	}
}
