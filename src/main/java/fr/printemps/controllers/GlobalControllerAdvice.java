package fr.printemps.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import fr.printemps.entities.User;
import fr.printemps.services.UserService;

@ControllerAdvice
public class GlobalControllerAdvice {
	
	@Autowired
	UserService us;
	
	@ModelAttribute
	public void myMethod(Model model, Authentication auth) {
		if(auth != null && auth.isAuthenticated()) {
			Optional<User> u = us.findByUsername(auth.getName());
			if(u.isPresent()) {
				model.addAttribute("monCompte", u.get());
			}
		}
	}
}
