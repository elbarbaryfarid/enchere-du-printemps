package fr.printemps.controllers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.printemps.entities.Enchere;
import fr.printemps.entities.Lot;
import fr.printemps.entities.LotImage;
import fr.printemps.services.LotService;
import fr.printemps.services.UserService;
import fr.printemps.storage.files.StorageService;
import fr.printemps.tools.DtoTool;
import jakarta.validation.Valid;

@Controller
@RequestMapping("/lots")
public class LotController {

	@Autowired
	private LotService ls;

	@Autowired
	private UserService userService;

	@Autowired
	private StorageService storageService;

	@GetMapping(value = { "/{orderBy}", "","/" })
	public String getAll(@PathVariable(value = "orderBy", required = false) String orderBy, Model model) {
		
		List<Lot> lots = ls.getAllLots(orderBy);
		System.out.println(lots);
		model.addAttribute("lots", lots);
		return "lots";
	}

	@GetMapping(value = "/detail/{id}")
	public String getLotById(@PathVariable("id") Integer id, Model model) {
		/*
		 * 
		 * LotDto lot =
		 * restWithExceptionHandler.getForEntity(BASE_URL+"/lots/detail/"+id,
		 * LotDto.class).getBody(); EnchereDto[] results =
		 * restWithExceptionHandler.getForEntity(BASE_URL+"/enchere/"+id,
		 * EnchereDto[].class).getBody(); UserDto vendeur =
		 * restWithExceptionHandler.getForEntity(BASE_URL+"/api/v1/users/"+lot.
		 * getVendeurUsername(), UserDto.class).getBody();
		 * 
		 * model.addAttribute("lot", lot); model.addAttribute("encheres", results);
		 * model.addAttribute("vendeur", vendeur); return "detail_lot";
		 */
		Lot lot = ls.getLotByIdOrderEncheres(id).get();
		model.addAttribute("lot", lot);
		return "detail_lot";
	}

	@GetMapping(value = {"/add","/add/"})
	public String toLotForm(Model model) {
		model.addAttribute("dateMin", Date.from(Instant.now().plus(1, ChronoUnit.DAYS)).toString());
		return "lotForm";

	}

	@PostMapping(value = "/add")
	public String addLot(@RequestParam("images") MultipartFile[] images,
			@Valid @ModelAttribute("formData") Lot lotForm, BindingResult bindingResult, Model model, Authentication authentication) {
		try {
			lotForm.setVendeur(userService.findByUsername(authentication.getName()).get());
			lotForm.setDateCreation(LocalDateTime.now());
			for (MultipartFile image : images) {
				if(!image.isEmpty()) {
					String fileLocation = storageService.store(image);
					LotImage img = new LotImage();
					img.setLien(fileLocation);
					img.setLot(lotForm);
					lotForm.getImagesPath().add(img);
				}
			}
			ls.saveLot(lotForm);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/lots";
	}

	@PostMapping("/{id}/encherir")
	public String encherir(@PathVariable("id") Integer id, Enchere enchere, Model model,
			Authentication auth) {
		enchere.setAcheteur(userService.findByUsername(auth.getName()).get());
		Optional<Lot> lot = ls.getLotById(id);
		if (lot.isPresent()) {
			Lot leLot = lot.get();
			enchere.setLot(leLot);
			enchere.setDateCreation(LocalDateTime.now());
			leLot.getEncheres().add(enchere);
			ls.saveLot(leLot);
		}
		return "redirect:/lots/detail/"+id.toString();
	}
	/*
	 * @PostMapping(value = "/addEnchere") public String
	 * addEnchere(@Valid @ModelAttribute("formData") EnchereDto enchere,
	 * BindingResult bindingResult, Model model) {
	 * System.out.println("FRONT : LotController.addEnchere " + enchere);
	 * enchere.setDateCreation(Date.from(Instant.now())); try {
	 * //RequestEntity<EnchereDto> requete = RequestEntity.post(new
	 * URI(BASE_URL+"/enchere")).contentType(MediaType.APPLICATION_JSON).body(
	 * enchere); //System.out.println(restWithExceptionHandler.exchange(requete,
	 * LotDto.class)); } catch (URISyntaxException e) { e.printStackTrace(); }
	 * return "redirect:/lots/"+enchere.getLotId(); }
	 */
}
