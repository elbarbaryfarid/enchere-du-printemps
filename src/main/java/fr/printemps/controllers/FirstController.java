package fr.printemps.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fr.printemps.entities.Lot;
import fr.printemps.services.LotService;
import fr.printemps.tools.DtoTool;

@Controller
public class FirstController {

	@Autowired
	private LotService ls;
	
//	@Autowired
//	private RestTemplate restTemplate;
//
//	private final String BASE_URL = "http://localhost:8084";
//	
//	@GetMapping("/")
//	public String afficherLots(Model model) {
//		List<LotDto> lots = Arrays.asList(restTemplate.getForEntity(BASE_URL+"/lot", LotDto[].class).getBody());
//		model.addAttribute("lots", lots);
//		return "PageAccueil";
//	}	
//	@GetMapping("/lots/{id}")
//	public String afficherDetailLot(@PathVariable("id") int id, Model model) {
//		LotDto lot = restTemplate.getForEntity(BASE_URL+"/lot/detail/"+id, LotDto.class).getBody();
//		//if (lot.isPresent()) {
//		model.addAttribute("lot", lot);
//		return "detail_lot";
//		/*
//		} else {
//			// le cas où le lot n'existe pas
//			return "PageAccueil"; // redirection vers la liste des lots
//		}*/
//	}
	
	@GetMapping("/")
	public String afficherAccueil(Model model) {
		List<Lot> lots = ls.getAllLots(null);
		model.addAttribute("lots", lots);
		return "PageAccueil";
	}
	
}
