package fr.printemps.controllers;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.printemps.entities.User;
import fr.printemps.repositories.UserRepository;
import fr.printemps.services.UserService;
import fr.printemps.storage.files.StorageFileNotFoundException;
import fr.printemps.storage.files.StorageService;

@RestController
@RequestMapping("api/file")
public class FileController {

	@Autowired
	private StorageService storageService;

	@GetMapping("/")
	public List<String> listUploadedFiles() throws IOException {
		List<String> retElements = storageService.loadAll().map(
				path -> MvcUriComponentsBuilder.fromMethodName(FileController.class,
						"serveFile", path.getFileName().toString()).build().toUri().toString())
				.collect(Collectors.toList());
		
		return retElements;
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}

	@PostMapping("/")
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
		
		if(file.getContentType().startsWith("image/")) {
			String fileLocation = storageService.store(file);
			fileLocation = fileLocation.substring(fileLocation.lastIndexOf('\\')+1,fileLocation.length());
			System.out.println(fileLocation);
			return ResponseEntity.ok(fileLocation);

		}
//		redirectAttributes.addFlashAttribute("message",
//				"You successfully uploaded " + file.getOriginalFilename() + "!");
		ResponseEntity<String> response = new ResponseEntity<String>("Mauvais type de fichier envoyé", HttpStatusCode.valueOf(400));
		return response;
	}
	
	@PostMapping("/multi")
	public ResponseEntity<List<String>> handleMultipleFileUpload(@RequestParam("images") MultipartFile[] files) {
		List<String> filesLocations = new ArrayList<>();
		for(MultipartFile file : files ) {
			if(file.getContentType().startsWith("image/")) {
				String fileLocation = storageService.store(file);
				filesLocations.add(fileLocation);
			}
		}
		return ResponseEntity.ok(filesLocations);
	}
	
	@PostMapping("/{lot}/pics/remove")
	public ResponseEntity<String> handleLotPicRemove(){
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	@PostMapping("/{lot}/pics/add")
	public ResponseEntity<String> handleLotPicAdd(){
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

}