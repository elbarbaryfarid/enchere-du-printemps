package fr.printemps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.printemps.entities.Lot;
@Repository
public interface LotRepository extends JpaRepository<Lot, Integer> {}
