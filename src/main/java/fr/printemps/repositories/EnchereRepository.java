package fr.printemps.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.printemps.entities.Enchere;
import fr.printemps.entities.EnchereId;

@Repository
public interface EnchereRepository extends JpaRepository<Enchere, EnchereId>{

	public List<Enchere> findByLotId(int id);
	public List<Enchere> findByLotIdOrderByMontantDesc(int id);

}
