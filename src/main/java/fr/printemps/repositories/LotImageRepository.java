package fr.printemps.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.printemps.entities.LotImage;

@Repository
public interface LotImageRepository extends JpaRepository<LotImage, Integer>{

}
