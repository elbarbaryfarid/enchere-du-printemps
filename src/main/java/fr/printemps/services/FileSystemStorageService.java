package fr.printemps.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import fr.printemps.storage.files.StorageException;
import fr.printemps.storage.files.StorageFileNotFoundException;
import fr.printemps.storage.files.StorageProperties;
import fr.printemps.storage.files.StorageService;

/**
 * Service dédié au stockage sur système de fichier classique
 */
public class FileSystemStorageService implements StorageService {

	private final Path rootLocation;

	public FileSystemStorageService(StorageProperties properties) {
		this.rootLocation = Paths.get(properties.getLocation());
		init();
	}
	
	@Override
	public void init() {
		try {
			Files.createDirectories(rootLocation);
		}
		catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}

	@Override
	public String store(MultipartFile file) {
		String destinationRet = "";
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file.");
			}
			String originalName = file.getOriginalFilename();
	        String fileExtension = originalName.substring(originalName.lastIndexOf("."));
			String generatedName = UUID.randomUUID().toString() + fileExtension;
	        
	        File destinationFile = new File(Paths.get(this.rootLocation.toString()).toAbsolutePath().toString() + "\\" +generatedName);
	        while (destinationFile.exists()) {
	        	generatedName = UUID.randomUUID().toString() + fileExtension;
	            destinationFile = new File(Paths.get(this.rootLocation.toString()).toAbsolutePath().toString() + generatedName);
	        }
			Path destinationPath = destinationFile.toPath();
			if (!destinationPath.getParent().toAbsolutePath().equals(this.rootLocation.toAbsolutePath())) {
				// This is a security check
				throw new StorageException(
						"Cannot store file outside current directory.");
			}
			try (InputStream inputStream = file.getInputStream()) {
				Files.copy(inputStream, destinationPath,
					StandardCopyOption.REPLACE_EXISTING);
				destinationRet = generatedName;
			}
		}
		catch (IOException e) {
			throw new StorageException("Failed to store file.", e);
		}
		return destinationRet;
	}

	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation.toAbsolutePath(), 1)
				.filter(path -> !path.equals(this.rootLocation.toAbsolutePath()))
				.map(this.rootLocation::relativize);
		}
		catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}
	}

	@Override
	public Path load(String filename) throws InvalidPathException {
		return rootLocation.resolve(filename);
	}

	@Override
	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			}
			else {
				throw new StorageFileNotFoundException(
						"Could not read file: " + filename);

			}
		}
		catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

}
