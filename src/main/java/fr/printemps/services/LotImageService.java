package fr.printemps.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.printemps.entities.LotImage;
import fr.printemps.repositories.LotImageRepository;
import fr.printemps.tools.DtoTool;

@Service
public class LotImageService {
	private final LotImageRepository lotImageRepository;
	
	@Autowired
	public LotImageService(LotImageRepository lotImageRepository) {
		this.lotImageRepository = lotImageRepository;
	}
	
    public LotImage saveLotImage(LotImage lotImageDto) {
    	return lotImageRepository.save(lotImageDto);
    }
}
