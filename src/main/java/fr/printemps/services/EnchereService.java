package fr.printemps.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.printemps.entities.Enchere;
import fr.printemps.entities.EnchereId;
import fr.printemps.repositories.EnchereRepository;
import fr.printemps.repositories.LotRepository;
import fr.printemps.repositories.UserRepository;
import fr.printemps.tools.DtoTool;

@Service
public class EnchereService {

	@Autowired
	EnchereRepository er;
	
	@Autowired
	LotRepository lr;
	
	@Autowired
	UserRepository ur;
	
	public EnchereService(EnchereRepository er) {
		this.er = er;
	}
	
	public Enchere save(Enchere a) {
		Enchere e = DtoTool.convert(a, Enchere.class);
		System.out.println("EnchereService.save : "+a);
		//e.setAcheteur(ur.findById(a.getAcheteurId()).get());
		//e.setLot(lr.findById(a.getLotId()).get());

		
		Enchere saved = er.saveAndFlush(e);
		
		
		return saved;
	}

	public void delete(Enchere e) {
		er.delete(e);
	}

	public void delete(EnchereId id) throws Exception {
		er.deleteById(id);
	}

	public List<Enchere> findByLotId(int id) {
		List<Enchere> encheres = er.findByLotId(id);
		return encheres;
	}

	public List<Enchere> findByLotIdOrderByMontant(int id){
		List<Enchere> encheres = er.findByLotIdOrderByMontantDesc(id);
		return encheres;
//		List<EnchereDto> res = new ArrayList<>();
//		for(Enchere e : encheres) {
//			res.add(DtoTool.convert(e, EnchereDto.class));
//		}
//		return res;
	}
	
	public List<Enchere> findAll() {
		return er.findAll();
	}
	
}
