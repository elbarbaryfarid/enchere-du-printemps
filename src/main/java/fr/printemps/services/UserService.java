package fr.printemps.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.printemps.entities.User;
import fr.printemps.repositories.UserRepository;

@Service
public class UserService {
	private final UserRepository userRepository;
	
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	public List<User> getAllUsers(String orderBy){
		if(orderBy == null) {
			return userRepository.findAll();			
		}
		return userRepository.findAll(Sort.by(Sort.Direction.ASC,orderBy));
	}
	
	public Optional<User> findByUsername(String user){
		return userRepository.findByUsername(user);
	}
	
	public boolean setProfilPicture(String user, String path) {
		boolean ret = false;
		Optional<User> u = userRepository.findByUsername(user);
		if(u.isPresent()) {User leUser = u.get(); leUser.setProfilPicture(path);userRepository.save(leUser);ret=true;}
		return ret;
	}
	
	public User save(User user) {
		return userRepository.save(user);
	}
	
	
}
