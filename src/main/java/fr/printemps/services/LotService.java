package fr.printemps.services;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.printemps.entities.Enchere;
import fr.printemps.entities.Lot;
import fr.printemps.repositories.LotRepository;
import fr.printemps.tools.DtoTool;

@Service
public class LotService {
    private final LotRepository lotRepository;

    @Autowired
    public LotService(LotRepository lotRepository) {
        this.lotRepository = lotRepository;
    }
    
    public List<Lot> getAllLots(String orderBy) {
    	if(orderBy == null) {
            return lotRepository.findAll();
    	}
    	return lotRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy));
    }

    public Optional<Lot> getLotById(int id) {
        return lotRepository.findById(id);
    }
    
    public Optional<Lot> getLotByIdOrderEncheres(int id){
    	Optional<Lot> lot = lotRepository.findById(id);
    	if(!lot.isEmpty()) {
    		Comparator<Enchere> compareByMontant = (Enchere e1, Enchere e2) -> Double.compare(e2.getMontant(),e1.getMontant());
    		Collections.sort(lot.get().getEncheres(), compareByMontant);
    	}
    	return lot;
    }
    
    public Lot saveLot(Lot lot) {
    	return lotRepository.saveAndFlush(lot);
    }
    
}
