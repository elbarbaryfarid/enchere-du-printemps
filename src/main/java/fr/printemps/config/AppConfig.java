package fr.printemps.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.printemps.services.FileSystemStorageService;
import fr.printemps.storage.files.StorageProperties;
import fr.printemps.storage.files.StorageService;

@Configuration
public class AppConfig {

	@Bean
	StorageService initStorageService() {
		StorageService ret = new FileSystemStorageService(new StorageProperties());
		ret.init();
		return ret;
	}
}
