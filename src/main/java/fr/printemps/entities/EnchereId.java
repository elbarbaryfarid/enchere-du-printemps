package fr.printemps.entities;

public class EnchereId {

	private Lot lot;

	private User acheteur;
	
	public EnchereId(Lot lot, User acheteur) {
		super();
		this.lot = lot;
		this.acheteur = acheteur;
	}
	public EnchereId() {
		super();
	}
	public Lot getLot() {
		return lot;
	}
	public void setLot(Lot lot) {
		this.lot = lot;
	}
	public User getAcheteur() {
		return acheteur;
	}
	public void setAcheteur(User acheteur) {
		this.acheteur = acheteur;
	}
	@Override
	public String toString() {
		return "EnchereId [lot=" + lot + ", acheteur=" + acheteur + "]";
	}

	
	
}
