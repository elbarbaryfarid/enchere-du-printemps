package fr.printemps.entities;

import java.time.LocalDateTime;
import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.ManyToOne;

@Entity
@IdClass(EnchereId.class) // Précise une classe déclarative reprenant les éléments
public class Enchere {

	@Id
	@ManyToOne
	private User acheteur;

	@Id
	@ManyToOne
	private Lot lot;

	private double montant;

	@Column
	private LocalDateTime dateCreation;

	public Enchere() {
	}

	public Enchere(User acheteur, Lot lot, double montant, LocalDateTime date) {
		super();
		setAcheteur(acheteur);
		setLot(lot);
		setMontant(montant);
		setDateCreation(date);
	}

	public User getAcheteur() {
		return acheteur;
	}

	public void setAcheteur(User acheteur) {
		this.acheteur = acheteur;
	}

	public Lot getLot() {
		return lot;
	}

	public void setLot(Lot lot) {
		this.lot = lot;
	}

	public double getMontant() {
		return montant;
	}
	
	public long getMontantRoundToUp() {
		long ret = 0;
		try {
			ret = Math.round(montant);
		} catch(Exception e) {
			
		}
		return ret;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public LocalDateTime getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}

	@Override
	public String toString() {
		return "Enchere [acheteur=" + acheteur + ", lot=" + lot + ", montant=" + montant + ", dateCreation=" + dateCreation + "]";
	}

}
