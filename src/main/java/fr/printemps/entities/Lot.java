package fr.printemps.entities;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Lot {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@OneToMany(mappedBy="lot",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Enchere> encheres;
	private String titre;
	private String description;
	private Enum etat;
	private String matiere;
	@Column(columnDefinition = "double default 0")
	private double prixDepart;
	@Column(columnDefinition = "bit(1) default 0")
	private boolean terminee;
	@OneToMany(mappedBy="lot", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<LotImage> images;
	@ManyToOne
	private User vendeur;
	@Column
	private LocalDateTime dateCreation;
	@Column
	private LocalDateTime dateFin;
	
	
	public boolean isTerminee() {
		return terminee;
	}

	public void setTerminee(boolean terminee) {
		this.terminee = terminee;
	}

	public List<LotImage> getImages() {
		return images;
	}

	public void setImages(List<LotImage> images) {
		this.images = images;
	}

	public Lot() {
		super();
		this.images = new ArrayList<>();
	}
	
	public Lot(int id, String titre, String description, User vendeur, LocalDateTime dateFin,
			boolean disponibilite) {
		this();
		this.id = id;
		this.titre = titre;
		this.description = description;
		this.vendeur = vendeur;
		this.dateCreation = LocalDateTime.now();
		this.dateFin = dateFin;
	}
	
	public Lot(String titre, String description, User vendeur, LocalDateTime dateFin,
			boolean disponibilite) {
		this();
		this.titre = titre;
		this.description = description;
		this.vendeur = vendeur;
		this.dateCreation = LocalDateTime.now();
		this.dateFin = dateFin;

	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Enchere> getEncheres() {
		return encheres;
	}
	
	public void setEncheres(List<Enchere> encheres) {
		this.encheres = encheres;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Enum getEtat() {
		return etat;
	}
	public void setEtat(Enum etat) {
		this.etat = etat;
	}
	public String getMatiere() {
		return matiere;
	}
	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}
	public List<LotImage> getImagesPath() {
		return images;
	}
	public void setImagesPath(List<LotImage> imagesPath) {
		this.images = imagesPath;
	}
	public User getVendeur() {
		return vendeur;
	}
	public void setVendeur(User vendeur) {
		this.vendeur = vendeur;
	}
	public LocalDateTime getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}
	public LocalDateTime getDateFin() {
		return dateFin;
	}
	public void setDateFin(LocalDateTime dateFin) {
		this.dateFin = dateFin;
	}
	
	public boolean isDisponibilite() {
		if(dateFin == null)
			return true;
		return dateFin.isAfter(LocalDateTime.now());
	}

	public double getPrixDepart() {
		return prixDepart;
	}

	public void setPrixDepart(double prixDepart) {
		this.prixDepart = prixDepart;
	}
	
	public double getMaxEnchere() {
		double ret = 0;
		if(encheres.size()>0) {
			Optional<Enchere> ec = encheres.stream().max(Comparator.comparingDouble(Enchere::getMontant));
			if(ec.isPresent()) {
				ret = ec.get().getMontant();
			}
		}
		return ret;
	}
	
}
