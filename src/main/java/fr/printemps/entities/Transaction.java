package fr.printemps.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="transactions")
public class Transaction implements Serializable {
	
	private static final long serialVersionUID = 5252725831383887017L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	
	@ManyToOne
	private User user;
	
	private double montant;
	
	@Column(name = "date")
	private LocalDateTime date;
	
	@Column(name="description", length = 350)
	private String description; // exemple : "transaction enchère n°..."
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Transaction() {
		
	}
	
	public Transaction(long id, User origin, User destination,double montant, LocalDateTime date) {
		this.Id = id;
		this.user = origin;
		this.montant = montant;
		this.date = date;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getAmount() {
		return montant;
	}

	public void setAmount(double amount) {
		this.montant = amount;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setTransactionDate(LocalDateTime date) {
		this.date = date;
	}
	
	
}