package fr.printemps.entities;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="users")
public class User implements Serializable {

	private static final long serialVersionUID = -1164128126429643115L;
	
	@Id
	@Column(name = "username", unique = true)
	private String username;
	@Column
	private String email;
	@Column
	private String password;
	@Column(name="enabled")
	private Boolean enabled;
	@Column
	private String firstName;
	@Column
	private String lastName;
	
	@Column(columnDefinition = "DATE")
	private LocalDate dateInscription;
	
	@Column(nullable = true,name = "picture")
	private String profilPicture;

	@OneToMany(mappedBy = "vendeur", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Lot> ventes;
	@OneToMany(mappedBy = "acheteur", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Enchere> encheres;
	
//	@ManyToMany
//	private ArrayList<Lot> suivis;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Transaction> transactions;
	
	
	
	@OneToMany(mappedBy = "username", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Role> Authorisations;
	
	@Column(name = "solde", columnDefinition = "integer default 0")
	private double solde = 0;
	
	public User() {
		
	}
	
	public User(String username, String firstName, String lastName, LocalDate dateInscription, String profilPicture, double solde) {
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.profilPicture = profilPicture;
		this.solde = solde;
		this.dateInscription = dateInscription;
	}

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String pseudo) {
		this.username = pseudo;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Lot> getVentes() {
		return ventes;
	}

	public void setMesVentes(List<Lot> mesVentes) {
		this.ventes = mesVentes;
	}

	public List<Enchere> getEncheres() {
		return encheres;
	}

	public void setEncheres(List<Enchere> encheres) {
		this.encheres = encheres;
	}

	public double getSolde() {
		
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public String getProfilPicture() {
		return profilPicture;
	}

	public void setProfilPicture(String profilPicture) {
		this.profilPicture = profilPicture;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Role> getRoles() {
		return Authorisations;
	}

	public void setRoles(Set<Role> roles) {
		Authorisations = roles;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public LocalDate getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(LocalDate dateInscription) {
		this.dateInscription = dateInscription;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public Set<Role> getAuthorisations() {
		return Authorisations;
	}

	public void setAuthorisations(Set<Role> authorisations) {
		Authorisations = authorisations;
	}

	public void setVentes(List<Lot> ventes) {
		this.ventes = ventes;
	}
	
	public List<Enchere> getEncheresEnCours() {
		return this.encheres.stream().filter(n -> n.getLot().isDisponibilite()).collect(Collectors.toList());
	}
	
	public List<Lot> getLotsEnCours() {
		return this.ventes.stream().filter(n -> n.isDisponibilite()).collect(Collectors.toList());
	}
	
	public List<Lot> getLotsTermines(){
		return this.ventes.stream().filter(n -> !n.isDisponibilite()).collect(Collectors.toList());
	}
	
}
