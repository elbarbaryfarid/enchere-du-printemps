package fr.printemps.entities;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(name = "authorities", uniqueConstraints = { @UniqueConstraint(columnNames = { "username", "authority" }) })
public class Role implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6701180016872054842L;

	public Role(String authority) {
		this.authority = authority;
	}

	public Role() {}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	// Relation Many-to-One avec la table USERS
	@ManyToOne
	@JoinColumn(name = "username", referencedColumnName = "username", nullable = false)
	private User username;

	@Column(name = "authority")
	private String authority;

	public User getUsername() {
		return username;
	}

	public void setUsername(User username) {
		this.username = username;
	}

	public String getAuthority() {
		return authority;
	}

	public void setRole(String authority) {
		this.authority = authority;
	}

}