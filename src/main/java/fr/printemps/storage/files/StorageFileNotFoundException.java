package fr.printemps.storage.files;

public class StorageFileNotFoundException extends StorageException {

	private static final long serialVersionUID = 8607401751558158820L;

	
	public StorageFileNotFoundException(String message) {
		super(message);
	}

	public StorageFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
