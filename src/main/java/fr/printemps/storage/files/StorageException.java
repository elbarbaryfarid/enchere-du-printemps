package fr.printemps.storage.files;

public class StorageException extends RuntimeException {
	private static final long serialVersionUID = 176000475326526284L;

	public StorageException(String message) {
		super(message);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
