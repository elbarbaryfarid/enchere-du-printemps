package fr.printemps.storage.files;


public class StorageProperties {

	/**
	 * Default location for storing files
	 */
	private String location = "upload-dir";

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}