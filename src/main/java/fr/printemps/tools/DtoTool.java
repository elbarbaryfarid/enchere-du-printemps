package fr.printemps.tools;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

public class DtoTool {

    private static ModelMapper mapper = new ModelMapper();

    /**
     * Méthode qui converti une entity en dto et vice-versa
     * @param <TSource> Type Source
     * @param <TDestination> Type destination
     * @param obj Objet à convertir
     * @param classe Type de l'objet converti
     * @return L'objet converti
     */
    public static <TSource, TDestination> TDestination convert (TSource obj, Class<TDestination> classe) {
        //Ajouter des règles personnalisées ici
        return mapper.map(obj, classe);
    }
    
	public static <T, B> List<B> convertListToDto(List<T> list, Class<B> destination){
		List<B> res = new ArrayList<>();
		for(T l : list) {
			res.add(DtoTool.convert(l, destination));
		}
		return res;
	}
}

