package fr.printemps.components;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import fr.printemps.entities.Enchere;
import fr.printemps.entities.Lot;
import fr.printemps.entities.Transaction;
import fr.printemps.entities.User;
import fr.printemps.services.LotService;
import fr.printemps.services.UserService;

@Component
public class ScheduledTasks {

	@Autowired
	private LotService ls;
	
	@Autowired
	private UserService us;

	@Scheduled(fixedDelay  = 60000)
	public void transactionalChange() {
		List<Lot> lots = ls.getAllLots(null);
		lots = lots.stream().filter(e -> (!e.isTerminee() && !e.isDisponibilite())).collect(Collectors.toList());
		for (Lot lot : lots) {
			List<Enchere> encheres = lot.getEncheres();
			if (encheres.size() > 0) {
				Optional<Enchere> ec = encheres.stream().max(Comparator.comparingDouble(Enchere::getMontant));
				if (ec.isPresent()) {
					Enchere enchere = ec.get();
					User acheteur = enchere.getAcheteur();
					User vendeur = lot.getVendeur();
					
					Transaction transaAchat = new Transaction();
					transaAchat.setTransactionDate(LocalDateTime.now());
					transaAchat.setUser(acheteur);
					transaAchat.setAmount(0-enchere.getMontant());
					transaAchat.setDescription("Achat du lot n°" + lot.getId() + "<br>Nom de l'annonce : " + lot.getTitre());
					acheteur.getTransactions().add(transaAchat);
					acheteur.setSolde(acheteur.getSolde()-enchere.getMontant());
					
					Transaction transaVente = new Transaction();
					transaVente.setTransactionDate(LocalDateTime.now());
					transaVente.setUser(vendeur);
					transaVente.setAmount(0+enchere.getMontant());
					transaVente.setDescription("Vente du lot n°" + lot.getId() + "<br>Nom de l'annonce : " + lot.getTitre());
					vendeur.getTransactions().add(transaVente);
					vendeur.setSolde(vendeur.getSolde()+enchere.getMontant());
					lot.setTerminee(true);
					us.save(acheteur);
					us.save(vendeur);
					ls.saveLot(lot);
				}
			}

		}
	}
}