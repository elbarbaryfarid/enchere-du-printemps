# Enchère du printemps



## Description

Projet réalisé en 2 semaines dans le cadre d'une POEI (Préparation Opérationnelle à l'Emploi Individuelle).

Enchère du printemps est un site d'enchères, destiné aux particuliers il permet en outre de poster des lots et d'enchérir sur des lots.

## Technologies utilisées
Nous avons utilisé la galaxie Spring afin de réaliser ce projet, il s'agit de notre premier projet conçu avec cette technologie.
Nous avons également employé une bibliothèque telle que Bootstrap pour concevoir nos vues, vue généré via le moteur de template Thymeleaf.

## Développeurs
Merci à Maxime Bountry pour sa contribution en tant que second développeur du projet.

## Status du projet
Développement terminé

## License
MIT License

Copyright (c) 2024

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
