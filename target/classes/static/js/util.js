window.addEventListener('DOMContentLoaded', event => {
	// Toggle the side navigation
	const boutons = document.querySelectorAll('.switch-btn');
	const contenus = document.querySelectorAll('.content-switch');
	if (boutons && contenus) {
		// Ajoutez des écouteurs d'événements pour les clics sur les boutons
		boutons.forEach((bouton, index) => {
			bouton.addEventListener('click', () => {
				// Masquez tous les éléments de contenu
				contenus.forEach((contenu) => {
					contenu.style.display = 'none';
				});

				boutons.forEach((bout) => {
					bout.classList.remove("active");
				});

				// Affichez uniquement le contenu correspondant au bouton cliqué
				contenus[index].style.display = '';
				bouton.classList.add("active");
			});
		});
	}/**
 * 
 */
})
