window.addEventListener('DOMContentLoaded', event => {
			var toRemove = "";
			function dateTiming(toDate, element) {
				var dateTo = new Date(toDate);
				setInterval(function(){
					dateApply(dateTo, element)
				} , 1000);
			}
			function dateApply(toDate, element) {
				var dateNow = new Date();
				var difference = toDate.getTime() - dateNow.getTime();
				const secs = Math.floor(Math.abs(difference) / 1000);
				var d = new Date(secs * 1000);
				var days = "";
				if(d.getDate() > 1){
					
					days = (d.getDate()-1) < 10 ? "0"+(d.getDate()-1) : (d.getDate()-1);
				}
				if(toRemove != ""){
					element.classList.remove(toRemove);
					toRemove = "";
				} else {
					element.classList.add("border-danger");
					toRemove = "border-danger";
				}
				element.querySelector(".diff").textContent = days == "" ? d.toISOString().substring(11, 19) : days + ":" + d.toISOString().substring(11, 19);
			}
	const timer = document.querySelector('.card-timer');
	if(timer){
		var date = timer.getAttribute("data-date");
		dateTiming(date, timer);
	}
});
