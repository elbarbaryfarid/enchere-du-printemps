/*Valeurs de bases UTILISATEUR*/
INSERT INTO users(username,password,first_name,last_name,solde,enabled, date_inscription, picture) VALUES ("1erepersonne","$2y$10$YnGHIIaieoMXNFgoIxcqs.SuhN0gZFYq1pWKA8Ar43C/j4WE9vO1a","Je","Jeu",50000.0,true, '2023-09-14', "");
INSERT INTO users(username,password,first_name,last_name,solde,enabled, date_inscription, picture) VALUES ("2emepersonne","pass1","Tu","Tue",30000.0,false, '2023-06-10', "");
INSERT INTO users(username,password,first_name,last_name,solde,enabled, date_inscription, picture) VALUES ("3emepersonne","pass1","Il","Ile",100000.0,false, '2022-01-01', "");
INSERT INTO users(username,password,first_name,last_name,solde,enabled, date_inscription, picture) VALUES ("4emepersonne","pass1","Heu","Heu",0,true, '2023-09-14', "");
INSERT INTO users(username,password,first_name,last_name,solde,enabled, date_inscription, picture) VALUES ("utilisateur1","$2a$10$FILwv6yYzGWn04u/D6u9UeD8YmtFhLDMsCyKi.KvgKkLObn0nxJ/q","farid","el barbary",30000,true, '2023-09-20', "");


/*Valeurs de bases Authority*/
INSERT INTO authorities(username, role) VALUES("1erepersonne", "USER"),("2emepersonne", "ADMIN")

/*Valeurs de bases LOT*/
INSERT INTO LOT(id,vendeur_username,titre,description, date_creation,date_fin) VALUES (1,"4emepersonne", "La Joconde", "Un tableau de Leonard de Vinci, mon dieu mais que fais-t-il là ???",'2020-10-10','2021-10-10');
INSERT INTO LOT(id,vendeur_username,titre,description, date_creation,date_fin) VALUES (2,"4emepersonne","Le Radeau de la Métdu", "Un tableau de Théodore Géricalt", "2023-09-18","2023-09-20 20:00:00");
INSERT INTO LOT(id,vendeur_username,titre,description, date_creation,date_fin) VALUES (3,"4emepersonne","Une chaise du VII siècle", "Mais genre juste une chaise, elle est même plus dispo à la vente :/",'2017-02-04','2018-12-12');

INSERT INTO `lot_image`(`lot_id`, `lien`) VALUES (1,'Mona_Lisa.jpg')
INSERT INTO `lot_image`(`lot_id`, `lien`) VALUES (2,'LeRadeauDeLaMeduse.jpg')

/*Valeurs de bases ENCHERES*/
INSERT INTO ENCHERE(lot_id,acheteur_username, montant, date_creation) VALUES (1,"1erepersonne", 20.0, '2020-11-11T17:00:00')
INSERT INTO ENCHERE(lot_id,acheteur_username, montant, date_creation) VALUES (1,"2emepersonne", 500.0, '2021-01-01T16:00:00')
INSERT INTO ENCHERE(lot_id,acheteur_username, montant, date_creation) VALUES (1,"3emepersonne", 666.66, '2021-05-05T01:23:45')

INSERT INTO ENCHERE(lot_id,acheteur_username, montant, date_creation) VALUES (2,"2emepersonne",17.6, '2017-03-12T21:09:54')
INSERT INTO ENCHERE(lot_id,acheteur_username, montant, date_creation) VALUES (2,"3emepersonne",6498.68, '2018-01-21T23:59:59')

INSERT INTO ENCHERE(lot_id,acheteur_username, montant, date_creation) VALUES (3,"2emepersonne", 0.1, '2017-03-20T11:11:11')